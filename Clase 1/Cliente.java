import java.net.Socket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;
import java.lang.System;

class Cliente
{
  // lee del DataInputStream todos los bytes requeridos

  static void read(DataInputStream f,byte[] b,int posicion,int longitud) throws Exception
  {
    while (longitud > 0)
    {
      int n = f.read(b,posicion,longitud);
      posicion += n;
      longitud -= n;
    }
  }

  public static void main(String[] args) throws Exception
  {
    Socket conexion = new Socket("localhost",50000);
    long time_s, time_e;
    int i;
    DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
    DataInputStream entrada = new DataInputStream(conexion.getInputStream());

    // enva un entero de 32 bits
    salida.writeInt(123);
    time_s = System.currentTimeMillis();
    // envia un numero punto flotante
/*
   for(i = 1; i <= 10000; i++)
    {
      salida.writeDouble(i*1.0);
    }
    time_e = System.currentTimeMillis();
    System.out.println("Tiempo de envio: "+ ( time_e - time_s ) +" milisegundos");
    */
    // envia una cadena
    salida.write("hola".getBytes());

    // recibe una cadena
    byte[] buffer = new byte[4];
    read(entrada,buffer,0,4);
    System.out.println(new String(buffer,"UTF-8"));
    // envia 5 numeros punto flotante
    ByteBuffer b = ByteBuffer.allocate(10000*8);
    time_s = System.currentTimeMillis();

    for(i = 1; i <= 10000; i++)
    {
      b.putDouble(i*1.0);
    //b.putDouble(1.2);
    //b.putDouble(1.3);
    //b.putDouble(1.4);
    //b.putDouble(1.5);
    }

    time_e = System.currentTimeMillis();
    System.out.println("Tiempo de envio: "+ ( time_e - time_s ) +" milisegundos");
    byte[] a = b.array();
    salida.write(a);

    salida.close();
    entrada.close();
    conexion.close();
  }
}
