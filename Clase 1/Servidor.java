import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;

class Servidor
{
  // lee del DataInputStream todos los bytes requeridos
  static void read(DataInputStream f,byte[] b,int posicion,int longitud) throws Exception
  {
    while (longitud > 0)
    {
      int n = f.read(b,posicion,longitud);
      posicion += n;
      longitud -= n;
    }
  }

  public static void main(String[] args) throws Exception
  {
    ServerSocket servidor = new ServerSocket(50000);

    Socket conexion = servidor.accept();

    DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
    DataInputStream entrada = new DataInputStream(conexion.getInputStream());
    long time_s, time_e;
    int i;
    double x;
    // recibe un entero de 32 bits
    int n = entrada.readInt();
    System.out.println(n);

    // recibe un numero punto flotante
    /*time_s = System.currentTimeMillis();
    for(i = 0; i<10000; i++)
    {
            x = entrada.readDouble();
            System.out.println(x);
    }
    time_e = System.currentTimeMillis();
    System.out.println("Tiempo de recepcion: "+ ( time_e - time_s ) +" milisegundos");
    */
    // recibe una cadena
    byte[] buffer = new byte[4];
    read(entrada,buffer,0,4);
    System.out.println(new String(buffer,"UTF-8"));

    // envia una cadena
    salida.write("HOLA".getBytes());

    // recibe 5 numeros punto flotante
    byte[] a = new byte[10000*8];
    read(entrada,a,0,10000*8);
    ByteBuffer b = ByteBuffer.wrap(a);
    time_s = System.currentTimeMillis();

      for (i = 0; i < 10000; i++)
      System.out.println(b.getDouble());

    time_e = System.currentTimeMillis();
    System.out.println("Tiempo de recepcion: "+ ( time_e - time_s ) +" milisegundos");
    salida.close();
    entrada.close();
    conexion.close();
  }
}
