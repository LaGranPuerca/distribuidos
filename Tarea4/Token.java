import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.nio.ByteBuffer;
import java.lang.Thread;

class Token
{
  static DataInputStream entrada;
  static DataOutputStream salida;
  static boolean primera_vez = true;
  static String ip;
  static long token = 0;
  static int nodo;

  static class Worker extends Thread
  {
    public void run()
    {
        try{
            ServerSocket servidor = new ServerSocket(50000);
            Socket conexion = servidor.accept();
            entrada = new DataInputStream(conexion.getInputStream());
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
        System.out.println("Server:" + (50000+nodo));
    }
  }

  public static void main(String[] args) throws Exception
  {
    if (args.length != 2)
    {
      System.err.println("Se debe pasar como parametros el numero de nodo y la IP del siguiente nodo");
      System.exit(1);
    }

    nodo = Integer.valueOf(args[0]);  // el primer parametro es el numero de nodo
    ip = args[1];  // el segundo parametro es la IP del siguiente nodo en el anillo
    token = 0;
    Worker w = new Worker();
    w.start();
    Socket conexion = null;
    for(;/*EVER*/;){
        try{
            conexion = new Socket(ip,50000);
            break;
        }catch(Exception e){
            Thread.sleep(1000);
        }
    }
    System.out.println("Cliente:"+(50000+(nodo+1)%4));
    try{
        salida = new DataOutputStream(conexion.getOutputStream());
        w.join();// Espera a que el tread termine

        for(;/*EVER*/;){
          if(nodo == 0 && primera_vez == true){
            if(primera_vez == true){
                primera_vez = false;
            }else{
              token = entrada.redLong();
            }
          }
          else{
              token = entrada.readLong();
          }
          token ++;
          System.out.println("Token: " + token);
          salida.writeLong(token);
        }
    }catch(Exception e){
        System.out.println(e.getMessage());
    }
  }
}
