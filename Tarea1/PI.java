/*Guzmán Fuentes David
 *Grupo: 4CM3 */

import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.lang.Thread;
import java.nio.ByteBuffer;

class PI {
	static Object lock = new Object();
	static double pi = 0;

	static class Worker extends Thread{
		Socket conexion;

		Worker(Socket conexion){
			this.conexion = conexion;
		}	
		public void run() {

			try{
				//abrimos los canales de comunicacion
				DataInputStream entrada = new DataInputStream(conexion.getInputStream());
				DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());

				double x = entrada.readDouble(); //suma parcial del nodo x
				synchronized(lock){
					pi += x;
				}
				
				//cerrar salida, entrada y el socket
				salida.close();
        		entrada.close();
        		conexion.close();
			}
			catch (Exception e){
        		System.err.println(e.getMessage());
      		}
		}
	}
	public static void main(String[] args) throws Exception {
		if(args.length != 1){
			System.err.println("Uso:");
			System.err.println("java PI <nodo>");
			System.exit(0);
		}
		int nodo = Integer.valueOf(args[0]);

		if (nodo == 0) {
			ServerSocket servidor = new ServerSocket(50000);
			//espera a que lleguen las conexiones
			Worker [] w = new Worker[3];
			for (int i=0; i<3; i++) {
				Socket conexion = servidor.accept();
				w[i] = new Worker(conexion);
				w[i].start();//echar a andar los Thread
			}

			double suma = 0;
			for(int i=0; i<10000000; i++)
				suma += 4.0/(8*i+1);

			synchronized(lock){
				pi += suma;
			}

			for (int i=0; i<3; i++)
				w[i].join();
			System.out.println(pi);
		}
		else {
			Socket conexion = new Socket("localhost", 50000);
			double suma = 0;
			DataInputStream entrada = new DataInputStream(conexion.getInputStream());
			DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());

			for (int i=0; i<10000000; i++)
				suma += 4.0 /(8*i+(nodo-1)*2+3);

			suma = ( nodo%2 ==0 ) ? suma : -suma;
			salida.writeDouble(suma);

			salida.close();
    		entrada.close();
    		conexion.close();
		}
	}	
}
