(TeX-add-style-hook
 "Reporte"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt" "twoside")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("babel" "spanish") ("srcltx" "active")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "inputenc"
    "babel"
    "amssymb"
    "amsmath"
    "srcltx"
    "amscd"
    "makeidx"
    "amsthm"
    "wrapfig"
    "graphicx"
    "tikz"))
 :latex)

