import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.io.BufferedReader;
import java.io.InputStreamReader;

class Chat
{
  static byte[] recibe_mensaje(MulticastSocket socket,int longitud) throws IOException
  {
    byte[] buffer = new byte[longitud];
    DatagramPacket paquete = new DatagramPacket(buffer,buffer.length);
    socket.receive(paquete);
    return buffer;
  }
  static void envia_mensaje(byte[] buffer,String ip,int puerto) throws IOException
  {
    DatagramSocket socket = new DatagramSocket();
    InetAddress grupo = InetAddress.getByName(ip);
    DatagramPacket paquete = new DatagramPacket(buffer,buffer.length,grupo,puerto);
    socket.send(paquete);
    socket.close();
  }
  static class Worker extends Thread
  {
    public void run()
    {
      try{
     // 230.0.0.0 a través del puerto 50000 y se desplegarán en la pantalla.
        InetAddress grupo = InetAddress.getByName("230.0.0.0");
        MulticastSocket socket = new MulticastSocket(50000);
        socket.joinGroup(grupo);
     // En un ciclo infinito se recibirán los mensajes enviados al grupo
        for(;/*EVER*/;){
          byte[] len = recibe_mensaje(socket,8);
          ByteBuffer b = ByteBuffer.wrap(len);
          byte[] buffer = recibe_mensaje(socket,b.getInt());
          System.out.println(new String(buffer,"UTF-8"));
        }
      }catch(IOException e){}
    }
  }
  public static void main(String[] args) throws Exception
  {
    if(args.length != 1){
      System.err.println("Se debe pasar como parametro el nombre de usuario");
      System.exit(1);
    }
    String nombre = args[0];
    Worker w = new Worker();
    w.start();

    BufferedReader b = new BufferedReader(new InputStreamReader(System.in));
    String mensaje = new String();
    // En un ciclo infinito se leerá los mensajes del teclado y se enviarán
    for(;/*EVER*/;){
      System.out.println("Escribe el mensaje: ");
      if((mensaje = b.readLine()) != null){
        byte[] buffer = (nombre+": "+mensaje).getBytes();
        ByteBuffer len = ByteBuffer.allocate(8);
        len.putInt(buffer.length);
        envia_mensaje(len.array(),"230.0.0.0",50000);
        envia_mensaje(buffer,"230.0.0.0",50000);
      }
    }
  }
}
