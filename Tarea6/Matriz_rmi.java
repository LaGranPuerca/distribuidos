import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;

public class Matriz_rmi extends UnicastRemoteObject implements Interface_rmi
{
  private static final long serialVersionUID = 1L;

  // el constructor default requiere reportar que puede producirse RemoteException
  public Matriz_rmi() throws RemoteException
  {
    super( );
  }

  public int[][] multiplicaMatriz(int[][] A, int[][] B, int N){
    int[][] C = new int[N/2][N/2];
    for(int i=0; i < N/2; i++){
      for(int j=0; j < N/2; j++){
        for(int k=0; k < N; k++){
          C[i][j] += A[i][k] * B[j][k];
        }
      }
    }
    return C;
  }
}
