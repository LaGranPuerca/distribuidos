import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Interface_rmi extends Remote
{
  public int[][] multiplicaMatriz(int[][] A, int[][] B, int N) throws RemoteException;
}
