import java.rmi.Naming;

public class Servidor_rmi
{
  public static void main(String[] args) throws Exception
  {
    String url = "rmi://localhost/multiplicacion";
    Matriz_rmi obj = new Matriz_rmi();

    // Rebinds the specified name to a new remote object
    Naming.rebind(url,obj);
  }
}
