import java.rmi.Naming;

public class Cliente
{
  static final int N = 4;//N = 500;
  static int[][] A;
  static int[][] B;
  static int[][] C;
  public static void main(String args[]) throws Exception
  {
    A = new int[N][N];
    B = new int[N][N];
    C = new int[N][N];
    // Returns a reference, a stub, for the remote object associated with the specified name

    Interface_rmi nodo0 = (Interface_rmi)Naming.lookup("rmi://localhost/multiplicacion");
    Interface_rmi nodo1 = (Interface_rmi)Naming.lookup("rmi://10.0.9.4/multiplicacion");
    Interface_rmi nodo2 = (Interface_rmi)Naming.lookup("rmi://10.0.9.6/multiplicacion");
    Interface_rmi nodo3 = (Interface_rmi)Naming.lookup("rmi://10.0.9.7/multiplicacion");

    for(int i=0; i < A.length; i++){
      for(int j=0; j < A.length; j++){
        A[i][j] = 2 * i - j;
        B[i][j] = 2 * i + j;
      }
    }

    trasponer(B);
    int[][] A1 = parte_matriz(A,0);
    int[][] A2 = parte_matriz(A,N/2);
    int[][] B1 = parte_matriz(B,0);
    int[][] B2 = parte_matriz(B,N/2);
    int[][] C1 = nodo0.multiplicaMatriz(A1,B1,N);
    int[][] C2 = nodo1.multiplicaMatriz(A1,B2,N);
    int[][] C3 = nodo2.multiplicaMatriz(A2,B1,N);
    int[][] C4 = nodo3.multiplicaMatriz(A2,B2,N);
    acomodaMatriz(C, C1, 0, 0);
    acomodaMatriz(C, C2, 0, N);
    acomodaMatriz(C, C3, N/2,0);
    acomodaMatriz(C, C4, N/2, N/2);
    System.out.prinln("Matriz A");
    printMatriz(A);
    System.out.prinln("Matriz B transpuesta");
    printMatriz(B);
    System.out.prinln("Matriz C");
    printMatriz(C);
    checksum(C);
  }


  public static void checksum(int[][] C){
      long sum = 0;
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    sum += C[i][j];
                }
            }
            //Desplegar el checksum de la matriz C
            System.out.println("Checksum: " + sum);
    }
  static int[][] parte_matriz(int[][] A,int inicio)
  {
    int[][] M = new int[N/2][N];
    for (int i = 0; i < N/2; i++)
      for (int j = 0; j < N; j++)
        M[i][j] = A[i + inicio][j];
    return M;
  }

  public static void acomodaMatriz(int[][] C, int[][] Submat, int x, int y){
    for(int i=0; i < Submat.length; i++){
      for(int j=0; j < Submat.length; j++){
        C[i+x][j+y] = Submat[i][j];
      }
    }
  }

  public static void printMatriz(int[][] mat){
    for(int i=0; i < mat.length; i++){
      for(int j=0; j < mat[0].length; j++){
        System.out.print(mat[i][j] + "\t");
      }
      System.out.println();
    }
  }

  public static void trasponer(int[][] Mat){
    int aux;
    for(int i=0; i < Mat.length; i++){
      for(int j=0; j < i; j++){
        aux = Mat[i][j];
        Mat[i][j] = Mat[j][i];
        Mat[j][i] = aux;
      }
    }
  }
}
