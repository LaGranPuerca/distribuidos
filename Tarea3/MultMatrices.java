//David Guzmán Fuentes
import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.lang.Thread;
import java.nio.ByteBuffer;

public class MultMatrices {
    static int N = 1000;
    static double A[][];
    static double B[][];
    static double C[][];
    static int n = 0;
    static Object lock = new Object();


    static class Worker extends Thread{
	    Socket conexion;

        Worker(Socket conexion){
            this.conexion = conexion;
	    }

	    public void run() {
            try{
                DataInputStream entrada = new DataInputStream(conexion.getInputStream());
                DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
                int nodo = entrada.readInt();
                if(nodo == 1){
                    // Enviar la matriz A1 al nodo 1.
                    for(int i = 0; i < N/2; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(A[i][j]);
                        }
                    }
                    // Enviar la matriz B1 al nodo 1.
                    for(int i = 0; i < N/2; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(B[i][j]);
                        }
                    }
                    // Recibir la matriz C1
                    for(int i = 0; i < N/2; i++){
                        for(int j = 0; j < N/2; j++){
                            C[i][j] = entrada.readDouble();
                        }
                    }
                }else if(nodo == 2){
                    // Enviar la matriz A1 al nodo 2.
                    for(int i = 0; i < N/2; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(A[i][j]);
                        }
                    }
                    // Enviar la matriz B2 al nodo 2.
                    for(int i = N/2; i < N; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(B[i][j]);
                        }
                    }
                    for(int i = 0; i < N/2; i++){
                        for(int j = N/2; j < N; j++){
                            C[i][j] = entrada.readDouble();
                        }
                    }
                } else if(nodo == 3){
                    // Enviar la matriz A2 al nodo 3.
                    for(int i = N/2; i < N; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(A[i][j]);
                        }
                    }
                    // Enviar la matriz B1 al nodo 3.
                    for(int i = 0; i < N/2; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(B[i][j]);
                        }
                    }
                    for(int i = N/2; i < N; i++){
                        for(int j = 0; j < N/2; j++){
                            C[i][j] = entrada.readDouble();
                        }
                    }
                }else if(nodo == 4){
                    // Enviar la matriz A2 al nodo 4.
                    for(int i = N/2; i < N; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(A[i][j]);
                        }
                    }
                    // Enviar la matriz B2 al nodo 4.
                    for(int i = N/2; i < N; i++){
                        for(int j = 0; j < N; j++){
                            salida.writeDouble(B[i][j]);
                        }
                    }
                    for(int i = N/2; i < N; i++){
                        for(int j = N/2; j < N; j++){
                            C[i][j]=entrada.readDouble();
                        }
                    }
                }
                //Incrementar la variable "n" sincronizando mediante el objeto "lock".
                synchronized(lock){
                    n++;
                }
                //Cerrar la conexión y los streams de entrada y salida.
                salida.close();
                entrada.close();
                conexion.close();
            } catch (Exception e){
                System.err.println(e.getMessage());
            }
        }
    }

    public static void main(String[] args) throws Exception {
        // Verificar que el número de nodo pase como parámetro al programa.
        if(args.length != 1){
            System.err.println("Se espera el numero de nodo");
            System.exit(-1);
        }
        int nodo = Integer.valueOf(args[0]);
        if(nodo == 0){

            A = new double[N][N];
            B = new double[N][N];
            C = new double[N][N];

            // inicializa las matrices A y B
            for (int i = 0; i < N; i++)
                for (int j = 0; j < N; j++)
                    {
                        A[i][j] = 2 * i + j;
                        B[i][j] = 2 * i - j;
                    }

            // transpone la matriz B, la matriz traspuesta queda en B
            for (int i = 0; i < N; i++)
                for (int j = 0; j < i; j++)
                    {
                        double x = B[i][j];
                        B[i][j] = B[j][i];
                        B[j][i] = x;
                    }

            ServerSocket servidor = new ServerSocket(50000);
            //Esperar la conexión de 4 nodos dentro de un ciclo
            for (int i=0; i<4; i++) {
                Socket conexion = servidor.accept();
                Worker w = new Worker(conexion);
                w.start();
            }

            synchronized(lock){
                n++;
            }
            //Esperar a que la variable "n" sea 4
            for (; /*EVER*/; ) {
                synchronized(lock){
                if(n == 4)
                    break;
                }
                Thread.sleep(100);
            }
/*
            System.out.println("Matriz A");
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    System.out.print(A[i][j] + "\t");
                }
                System.out.println();
            }

            System.out.println("Matriz B");
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    System.out.print(B[i][j] + "\t");
                }
                System.out.println();
            }

            System.out.println("Matriz C");
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    System.out.print(C[i][j] + "\t");
                }
                System.out.println();
            }
  */          //Calcular el checksum de la matriz C.
            double sum = 0;
            for(int i = 0; i < N; i++){
                for(int j = 0; j < N; j++){
                    sum += C[i][j];
                }
            }
            //Desplegar el checksum de la matriz C
            System.out.println("Checksum: " + sum);
        }else{
            A = new double[N/2][N];     // la mitad de los renglones
            B = new double[N/2][N];
            C = new double[N/2][N/2];

            Socket conexion = new Socket("localhost", 50000);

            DataInputStream entrada = new DataInputStream(conexion.getInputStream());
            DataOutputStream salida = new DataOutputStream(conexion.getOutputStream());
            salida.writeInt(nodo);
            //Recibir la matriz A1 o A2 del nodo 0
            for(int i = 0; i < N/2; i++){
                for(int j = 0; j < N; j++){
                    A[i][j] = entrada.readDouble();
                }
            }

            //Recibir la matriz B1 o B2 del nodo 0
            for(int i = 0; i < N/2; i++){
                for(int j = 0; j < N; j++){
                    B[i][j] = entrada.readDouble();
                }
            }
            //Realizar la multiplicacion
            for(int i = 0; i < N/2; i++){
                for(int j = 0; j < N/2; j++){
                    for(int k = 0; k < N; k++){
                        C[i][j] += A[i][k] * B[j][k];
                    }
                }
            }
            //Enviar la matriz resultante al nodo 0
            for(int i = 0; i < N/2; i++){
                for(int j = 0; j < N/2; j++){
                    salida.writeDouble(C[i][j]);
                }
            }

            salida.close();
            entrada.close();
        }
    }
}
