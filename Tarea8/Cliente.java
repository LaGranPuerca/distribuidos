import java.io.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.sql.Timestamp;
import java.net.*;

public class Cliente{

static class Usuario{

      String email;
      String nombre;
      String apellido_paterno;
      String apellido_materno;
      String fecha_nacimiento;
      String telefono;
      String genero;
      byte[] foto;

    public Usuario(){}
    public Usuario(String email, String nombre, String apellido_paterno, String apellido_materno, String fecha_nacimiento, String telefono, String genero, byte[] foto){
        this.email = email;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.fecha_nacimiento = fecha_nacimiento;
        this.telefono = telefono;
        this.genero = genero;
        this.foto = null;
    }
    public Usuario(String email, String nombre, String apellido_paterno, String apellido_materno, String fecha_nacimiento, String telefono, String genero){
        this.email = email;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
        this.fecha_nacimiento = fecha_nacimiento;
        this.telefono = telefono;
        this.genero = genero;
        this.foto = null;
    }

}
    public static void main(String[] args) throws Exception{

        String email;
        String nombre;
        String apellido_paterno;
        String apellido_materno;
        String fecha_nacimiento;
        String telefono;
        String genero;
        byte[] foto=null;
        String respuesta;
        String parametros;

        URL url;
        boolean verificar = false;
        HttpURLConnection conexion;
        OutputStream os ;

        Gson j = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
        do{

        //MENU
        System.out.println("\nMENU");
        System.out.println("a. Alta usuario");
        System.out.println("b. Consulta usuario");
        System.out.println("c. Borra usuario");
        System.out.println("d. Borra todos los usuarios");
        System.out.println("e. Salir");
        System.out.print("Opcion:");

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader (isr);

        String cadena = (String)br.readLine();
        char opcion = cadena.charAt(0);
        switch(opcion){
            case 'a':
            System.out.println("\nAlta usuario");
            System.out.print("Email:");
            email = (String)br.readLine();

            System.out.print("Nombre:");
            nombre = (String)br.readLine();

            System.out.print("Apellido paterno:");
            apellido_paterno = (String)br.readLine();

            System.out.print("Apellido materno:");
            apellido_materno = (String)br.readLine();

            System.out.print("Fecha de nacimiento (yymmdd):");
            fecha_nacimiento = (String)br.readLine();

            System.out.print("Telefono:");
            telefono = (String)br.readLine();

            System.out.print("Genero (M)(F):");
            genero = (String)br.readLine();

            foto = null;

            Usuario usuario = new Usuario(email,nombre,apellido_paterno,apellido_materno,fecha_nacimiento,telefono,genero);

            String s = j.toJson(usuario);


            url = new URL("http://40.83.13.98:8080/Servicio/rest/ws/alta");

            conexion = (HttpURLConnection) url.openConnection();

            conexion.setDoOutput(true);

            // se utiliza el método HTTP POST (ver el método en la clase Servicio.java)
            conexion.setRequestMethod("POST");

            // indica que la petición estará codificada como URL
            conexion.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

            parametros = "usuario=" + URLEncoder.encode(s,"UTF-8");

            os = conexion.getOutputStream();

            os.write(parametros.getBytes());

            os.flush();

            // se debe verificar si hubo error
            if (conexion.getResponseCode() != HttpURLConnection.HTTP_OK)
                throw new RuntimeException("Codigo de error HTTP: " + conexion.getResponseCode());

            br = new BufferedReader(new InputStreamReader((conexion.getInputStream())));


            // el método web regresa una string en formato JSON
            while ((respuesta = br.readLine()) != null) System.out.println(respuesta);

            conexion.disconnect();
            //Fn de invicación al servicio
            break;

        case 'b':
            System.out.println("\nConsulta usuario");
            System.out.print("Email:");
            email = (String)br.readLine();

            url = new URL("http://40.83.13.98:8080/Servicio/rest/ws/consulta");

            conexion = (HttpURLConnection) url.openConnection();

            conexion.setDoOutput(true);

            conexion.setRequestMethod("POST");

            conexion.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

            parametros = "email=" + URLEncoder.encode(email,"UTF-8");

            os = conexion.getOutputStream();

            os.write(parametros.getBytes());

            os.flush();

            if (conexion.getResponseCode() != HttpURLConnection.HTTP_OK)
                throw new RuntimeException("Codigo de error HTTP: " + conexion.getResponseCode());

            br = new BufferedReader(new InputStreamReader((conexion.getInputStream())));

            String auxiliar="";
            while ((respuesta = br.readLine()) != null){
                if(respuesta != null){
                    auxiliar += respuesta;
                }
            }

            Usuario usr = new Usuario();

            usr = (Usuario)j.fromJson(auxiliar,Usuario.class);

            System.out.println("\nNombre:"+usr.nombre);
            System.out.println("apellido paterno:"+usr.apellido_paterno);
            System.out.println("apellido materno:"+usr.apellido_materno);
            System.out.println("fecha de nacimiento:"+usr.fecha_nacimiento);
            System.out.println("telefono:"+usr.telefono);
            System.out.println("genero:"+usr.genero);

            conexion.disconnect();
        break;

        case 'c':
            System.out.println("\nBorra usuario");
            System.out.print("Email:");
            email = (String)br.readLine();
            url = new URL("http://40.83.13.98:8080/Servicio/rest/ws/borra");

            conexion = (HttpURLConnection) url.openConnection();

            conexion.setDoOutput(true);

            conexion.setRequestMethod("POST");

            conexion.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

            parametros = "email=" + URLEncoder.encode(email,"UTF-8");

            os = conexion.getOutputStream();

            os.write(parametros.getBytes());

            os.flush();

            if (conexion.getResponseCode() != HttpURLConnection.HTTP_OK)

            throw new RuntimeException("Codigo de error HTTP: " + conexion.getResponseCode());

            br = new BufferedReader(new InputStreamReader((conexion.getInputStream())));


            while ((respuesta = br.readLine()) != null) System.out.println(respuesta);

            conexion.disconnect();
        break;
        case 'd':
            System.out.println("\nBorra todos los usuarios");
            url = new URL("http://40.83.13.98:8080/Servicio/rest/ws/borra_usuarios");

            conexion = (HttpURLConnection) url.openConnection();

            conexion.setDoOutput(true);

            conexion.setRequestMethod("POST");

            conexion.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

            parametros = "";
            os = conexion.getOutputStream();

            os.write(parametros.getBytes());

            os.flush();

            if (conexion.getResponseCode() != HttpURLConnection.HTTP_OK)
		throw new RuntimeException("Codigo de error HTTP: " + conexion.getResponseCode());

            br = new BufferedReader(new InputStreamReader((conexion.getInputStream())));

            while ((respuesta = br.readLine()) != null) System.out.println(respuesta);

            conexion.disconnect();
        break;
        case 'e':
            System.out.println("\nSalir");
            verificar = true;
        break;
        }
        }while(verificar != true);

    }

}
